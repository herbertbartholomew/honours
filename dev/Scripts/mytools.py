'''

Collection of useful functions

'''
import os


def reshape_OD_ouput(root, in_csv, length=True, time=True):
    import pandas as pd
    import os

    '''
    :param root: path to root of project
    :param in_csv: relative path (from root) to input csv
    :param length: flag to choose length matrix as an output
    :param time: flag to choose time matrix as an output
    :return: paths to output CSVs as list

    '''
    df = pd.read_csv(root + in_csv)

    print('MyTools: Loaded input CSV')
    lpath = root + r"\distMatRShp.csv"
    tpath = root + r"\timeMatRShp.csv"

    if length:
        distTab = pd.pivot(df, "MB_ID", "WIC_ID", "Total_Length")
        dist_namesDict = {}

        for col in distTab.columns:
            new_name = str("dist_" + col)
            dist_namesDict.update({col: new_name})

        distTab.rename(columns=dist_namesDict, inplace=True)

        if os.path.exists(lpath):
            os.remove(lpath)
        print('MyTools: Processing lengths matrix')
        distTab.to_csv(lpath)

    if time:
        timeTab = pd.pivot(df, "MB_ID", "WIC_ID", "Total_Minutes")
        time_namesDict = {}

        for col in timeTab.columns:
            new_name = str("time_" + col)
            time_namesDict.update({col: new_name})

        timeTab.rename(columns=time_namesDict, inplace=True)

        if os.path.exists(tpath):
            os.remove(tpath)

        print('MyTools: Processing times matrix')
        timeTab.to_csv(tpath)

    else:
        print('No accumulate value selected. Make sure to specify "Length" or "Time"')

    return [lpath, tpath]

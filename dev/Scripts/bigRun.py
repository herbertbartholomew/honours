'''

Goal: Run a series of scripts overnight to generate a results table for public transport

'''
import arcpy

arcpy.ImportToolbox(r"C:\Users\Michael\Downloads\TransitNetworkAnalysisTools_0.5.3\Transit Network Analysis Tools.pyt")

# Set environment variables
print('Configuring Workspace')

root = r"C:\GIS\honours\dev\gravity_model"
workspace = r"C:\GIS\honours\dev\gravity_model\public_transport.gdb"
arcpy.env.workspace = workspace
arcpy.env.scratchWorkspace = workspace


# Simulation Time Properties:

start_time = "06:30"  # 1hr before WICs open
end_time = "23:00"  # 1hr after WICs close
time_step = 30  # Compute results at 15min time intervals
# start_dates = ["20220428", "20220429", "20220430", "20220501"]  # Thursday -> Sunday during a 'normal' part of schedule
start_dates = ["20220430", "20220501"]

# Input OD Matrix
in_mat = root + r"\PT_OD_Matrix.lyrx"

# Main loop
for date in start_dates:
    print('Computing OD matrix for: ', date)
    stats = workspace + r"\travel_stats_" + date
    lines = workspace + r"\travel_lines_" + date

    arcpy.TransitNetworkAnalysisTools.CalculateTravelTimeStatistics(in_mat, stats, date, start_time, date, end_time, time_step, True, lines)

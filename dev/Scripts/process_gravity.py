"""
process_gravity.py

Run after dev_gravity_model.py

Version: v0
Date: 2022
Author: Michael Vernon
"""

########################################################################################################################
#
# Set local environment and processing variables
#
########################################################################################################################

import arcpy

# Set environment variables
print('Configuring Workspace')

root = r"C:\GIS\honours\dev\gravity_model"
workspace = root + r"\gravity_model.gdb"
arcpy.env.workspace = workspace
arcpy.env.scratchWorkspace = workspace
arcpy.env.overwriteOutput = True

# Set spatial reference. '4283' is the factory code for GCS_GDA_1994.
# Comes from https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/spatialreference.htm
spatialRefGDA94 = arcpy.SpatialReference(4283)
spatialRefMGAZone55 = arcpy.SpatialReference(28355)

########################################################################################################################
#
# Rasterize polygon results tables to generate combined model
#
########################################################################################################################

# Load up study area polygon

study_area = r'\ACT_census_FCs\ACT_abs_state'

# Load up results table

results = 'results'

# For each WIC, generate a raster

raster_dist_value_fields = [field.name for field in arcpy.ListFields(results, 'dist_likelihood_*')]
raster_time_value_fields = [field.name for field in arcpy.ListFields(results, 'time_likelihood_*')]

print('Generating rasters from results table')
for field in raster_dist_value_fields:

    # Create dataset to save raster, 10m cellsize
    cell = 10
    name = field + '_raster'
    print('Processing ', name)

    if arcpy.Exists(name):
        arcpy.Delete_management(name)

    arcpy.CreateRasterDataset_management(workspace, name, cellsize=cell,
                                         raster_spatial_reference=spatialRefMGAZone55)

    arcpy.PolygonToRaster_conversion(results, field, name, cell_assignment='CELL_CENTER', cellsize=cell)

for field in raster_time_value_fields:

    # Create dataset to save raster, 10m cellsize
    cell = 10
    name = field + '_raster'
    print('Processing ', name)

    if arcpy.Exists(name):
        arcpy.Delete_management(name)

    arcpy.CreateRasterDataset_management(workspace, name, cellsize=cell,
                                         raster_spatial_reference=spatialRefMGAZone55)

    arcpy.PolygonToRaster_conversion(results, field, name, cell_assignment='CELL_CENTER', cellsize=cell)

# Use cell statistics to combine the rasters into a single model

gravity_distance_rasters = arcpy.ListDatasets('dist_likelihood_*', 'Raster')
gravity_time_rasters = arcpy.ListDatasets('time_likelihood_*', 'Raster')

# Lesson learned = don't try use the raster calculator when there is a tool available directly.
cellStatsDist = arcpy.sa.CellStatistics(gravity_distance_rasters, 'MAXIMUM', 'NODATA')
cellStatsTime = arcpy.sa.CellStatistics(gravity_time_rasters, 'MAXIMUM', 'NODATA')

cellStatsDist.save("comb_dist")
cellStatsTime.save("comb_time")


def set_speed(road_type):
    if (road_type == "URBAN RESIDENTIAL 1" or road_type == "URBAN RESIDENTIAL 2" or road_type == "URBAN RESIDENTIAL 3" or road_type == "RURAL RESIDENTIAL" or road_type == "URBAN EXCLUSIVE USE"):
        return 50
    elif (road_type == "URBAN DISTRIBUTOR" or road_type == "RURAL SPECIAL PURPOSE"):
        return 60
    elif (road_type == "URBAN ARTERIAL" or road_type == "RURAL DISTRIBUTOR"):
        return 70
    elif (road_type == "HIGHWAYS" or road_type == "RURAL ARTERIAL"):
        return 80
    else:
        # Handle missing data. On visual inspection of ~20 roads out of 44k, they all looked like urban or industrial roads.
        return 50
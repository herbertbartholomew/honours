import arcpy
import time

# Flag to choose residential vs all meshblocks
useAllMeshblocks = False

# Start timer
rtime = time.time()

# Set environment variables
root = r"C:\GIS\honours\dev\gravity_model"
pt_workspace = root + r"\public_transport.gdb"
road_workspace = root + r"\gravity_model.gdb"

if useAllMeshblocks:
    grav_workspace = root + r"\pt_gravity.gdb"
else:
    grav_workspace = root + r"\pt_gravity_res.gdb"

arcpy.env.workspace = pt_workspace
arcpy.env.scratchWorkspace = pt_workspace
arcpy.env.overwriteOutput = True

# Create new GDB to store results of gravity modelling.

print("Creating database to store gravity model results at {}".format(grav_workspace))
if arcpy.Exists(grav_workspace):
    arcpy.Delete_management(grav_workspace)

if useAllMeshblocks:
    arcpy.CreateFileGDB_management(root, "pt_gravity.gdb")
else:
    arcpy.CreateFileGDB_management(root, "pt_gravity_res.gdb")


# Get stats tables
statsTables = arcpy.ListTables("travel_stats_*")

# Renaming key
dayDict = {
    "travel_stats_20220428": "Weekday",
    "travel_stats_20220429": "Friday",
    "travel_stats_20220430": "Saturday",
    "travel_stats_20220501": "Sunday",
}

# Copy each to the new GDB
print("Transferring PT stats tables to GDB")
for table in statsTables:

    table_copy = grav_workspace + r"\stats_copy_" + dayDict[table]
    arcpy.Copy_management(pt_workspace + "\\" + table, table_copy)

# Reset workspace to new GDB
arcpy.env.workspace = grav_workspace
arcpy.env.scratchWorkspace = grav_workspace

copiedTables = arcpy.ListTables()

# Map meshblock and WIC IDs onto stats tables:
if useAllMeshblocks:
    origins = road_workspace + r"\ACT_census_FCs\real_MB_XY"
else:
    origins = road_workspace + r"\ACT_census_FCs\manual_XY"

destinations = road_workspace + r"\wic_XY_FC"

print("Remapping IDs")
for table in copiedTables:
    # Transfer the MB ID from the input Origins to the output stats table
    arcpy.JoinField_management(table, "OriginID", origins, "ObjectID", ["MB_CODE21"])

    # Transfer the hospital name from the input Destinations to the output Lines
    arcpy.JoinField_management(table, "DestinationID", destinations, "ObjectID", ["disp_name", "ID"])

# Pivot to flatten results
print("Pivoting tables")
for table in copiedTables:
    mean_table = grav_workspace + r"\mean_pivot_" + table.split("_")[2]
    min_table = grav_workspace + r"\min_pivot_" + table.split("_")[2]
    max_table = grav_workspace + r"\max_pivot_" + table.split("_")[2]

    arcpy.PivotTable_management(table, "MB_CODE21", "ID", "Mean_PublicTransitTime", mean_table)
    arcpy.PivotTable_management(table, "MB_CODE21", "ID", "Min_PublicTransitTime", min_table)
    arcpy.PivotTable_management(table, "MB_CODE21", "ID", "Max_PublicTransitTime", max_table)

# Make copy of origin feature polygons to store results against

if useAllMeshblocks:
    origin_poly = road_workspace + r"\ACT_census_FCs\ACT_abs_MB"
else:
    origin_poly = road_workspace + r"\ACT_census_FCs\MB_manual_selection"

results_feature = grav_workspace + r"\pt_results"
arcpy.CopyFeatures_management(origin_poly, results_feature)

# Rename fields in each pivot table prior to joining to results feature

pivotTables = arcpy.ListTables("*_pivot_*")

print("Renaming and joining fields")
for table in pivotTables:

    table_name_parts = table.split("_")
    field_names = arcpy.ListFields(table, "wic_*")
    name_prefix = table_name_parts[2] + "_" + table_name_parts[0] + "_"

    field_list = []

    for field in field_names:
        new_field_name = name_prefix + field.name
        field_list.append(new_field_name)
        arcpy.AlterField_management(table, field.name, new_field_name, new_field_name)

    arcpy.JoinField_management(results_feature, "MB_CODE21", table, "MB_CODE21", field_list)

print("Computing gravity function")

all_fields = [f.name for f in arcpy.ListFields(results_feature, "*_wic_*")]

# This bit is horrendous. Need to refactor at some point in time.
# Basically takes the all_fields list which is entirely unstructured, and groups it into nested lists
# so that I can iterate through them and name fields in a more sensible manner.

weekday_fields = [match for match in all_fields if "Weekday" in match]
friday_fields = [match for match in all_fields if "Friday" in match]
saturday_fields = [match for match in all_fields if "Saturday" in match]
sunday_fields = [match for match in all_fields if "Sunday" in match]

weekday_min = [match for match in weekday_fields if "min" in match]
weekday_max = [match for match in weekday_fields if "max" in match]
weekday_mean = [match for match in weekday_fields if "mean" in match]

friday_min = [match for match in friday_fields if "min" in match]
friday_max = [match for match in friday_fields if "max" in match]
friday_mean = [match for match in friday_fields if "mean" in match]

saturday_min = [match for match in saturday_fields if "min" in match]
saturday_max = [match for match in saturday_fields if "max" in match]
saturday_mean = [match for match in saturday_fields if "mean" in match]

sunday_min = [match for match in sunday_fields if "min" in match]
sunday_max = [match for match in sunday_fields if "max" in match]
sunday_mean = [match for match in sunday_fields if "mean" in match]

weekday_group = [weekday_min, weekday_max, weekday_mean]
friday_group = [friday_min, friday_max, friday_mean]
saturday_group = [saturday_min, saturday_max, saturday_mean]
sunday_group = [sunday_min, sunday_max, sunday_mean]

all_group = [weekday_group, friday_group, saturday_group, sunday_group]

# Iterate through the nested lists to add gravity function fields, then compute gravity function
all_attract = []
for day_group in all_group:
    day_attract = []
    for stat_type in day_group:
        stat_attract = []
        for field in stat_type:
            # Add field for storing attractiveness
            new_field_name = field.replace("_wic", "_attract_wic")
            stat_attract.append(new_field_name)
            arcpy.AddField_management(results_feature, new_field_name, 'DOUBLE')

            expression = "compute_attract(!" + field + "!)"

                # Assume that all WICs equally attractive, and that gravity exponent is 2

            codeblock = '''
def compute_attract(time):
    principle = 1
    exponent = 2
    attract = principle / float(time) ** exponent
    return attract
'''
            arcpy.CalculateField_management(results_feature, new_field_name, expression, 'PYTHON3', codeblock)
        day_attract.append(stat_attract)
    all_attract.append(day_attract)

print('Computing gravity sum')
for day in all_attract:
    for stat in day:
        # Add total attractiveness columns
        new_field_name = stat[0].replace('attract_wic_1', 'total_attract')
        arcpy.AddField_management(results_feature, new_field_name, 'DOUBLE')

        # Generate expression that produces sum of attract columns
        expression = ''
        for field in stat:
            field = '!' + field + '!,'
            expression = expression + field

        expression = 'sum([' + expression.rstrip(',') + '])'
        # Compute total attractiveness column:
        arcpy.CalculateField_management(results_feature, new_field_name, expression, 'PYTHON3')

print('Solving likelihood')
# Add one field for likelihood for each WIC, and calculate that value:
for day in all_attract:
    for stat in day:
        # Extract name of sum field
        sum_name = stat[0].replace('attract_wic_1', 'total_attract')
        for field in stat:
            # Add likelihood field
            new_field_name = field.replace('attract', 'likelihood')
            arcpy.AddField_management(results_feature, new_field_name, 'DOUBLE')

            expression = '!' + field + '!/!' + sum_name + '!'
            # Compute Likelihood
            arcpy.CalculateField_management(results_feature, new_field_name, expression, 'PYTHON3')


print("Model complete. Total time %.0f s" % (time.time() - rtime))
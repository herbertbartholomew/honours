"""
model_import_data.py

Import select data to geo-database for gravity modelling.

Assumes you have already run the dev_import_data.py and dev_preprocess_data.py scripts, edited the attributes of the
network dataset to include travel time as an impedance function, and exported the selections to .xml files ready for
import here.

I don't think there is a way to access network dataset properties and config through python, so that part must be done
through GUI.

Version: v0
Date: 2022
Author: Michael Vernon
"""

import arcpy

# Set environment variables
workspace = r"C:\GIS\honours\dev\gravity_model\gravity_model.gdb"
arcpy.env.workspace = workspace
arcpy.env.scratchWorkspace = workspace
arcpy.env.overwriteOutput = True

# Set spatial reference. '4283' is the factory code for GCS_GDA_1994.
# Comes from https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/spatialreference.htm
spatialRefGDA94 = arcpy.SpatialReference(4283)
spatialRefMGAZone55 = arcpy.SpatialReference(28355)

# Create list of input files
input_base = r"C:\GIS\honours\dev\dev_proj\InputData"

wic_XY_shp = input_base + r"\dataset\wic_XY\wic_XY.shp"

input_shapes = [wic_XY_shp]

# Convert each shapefile to a feature class

for shape in input_shapes:
    # Get name of input variable to use to name outputs. Kinda hacky
    shape_name = [k for k, v in locals().items() if v == shape][0]
    print('processing shape: ' + shape_name)
    arcpy.FeatureClassToFeatureClass_conversion(shape, workspace, shape_name.rstrip("_shp") + "_FC")

# Import feature datasets - these have already been created in dev_ scripts

network_dataset = input_base + r"\dataset\network_dataset.xml"
act_features = input_base + r"\dataset\act_fcs.xml"

print('Importing Network')
arcpy.ImportXMLWorkspaceDocument_management(workspace, network_dataset)
print('Importing ACT features')
arcpy.ImportXMLWorkspaceDocument_management(workspace, act_features)
"""
dev_gravity_model.py

Run after model_import_data.py.

Version: v0
Date: 2022
Author: Michael Vernon
"""

########################################################################################################################
#
# Set local environment and processing variables
#
########################################################################################################################

import arcpy
import time
import mytools as mt

# start timer
rtime = time.time()

# Use this to choose real or dev data
dev_mode = False

# Use this to choose all meshblocks or filtered meshblocks
mb_filters = True
# filter_exp = "MB_CAT21 = 'Residential'"

if dev_mode:
    print('Dev mode selected. Model will be run for just 5 meshblocks')

if mb_filters:
    print('Meshblocks will be filtered')

# Set environment variables
print('Configuring Workspace')

root = r"C:\GIS\honours\dev\gravity_model"
workspace = r"C:\GIS\honours\dev\gravity_model\gravity_model.gdb"
arcpy.env.workspace = workspace
arcpy.env.scratchWorkspace = workspace
arcpy.env.overwriteOutput = True

# Set spatial reference. '4283' is the factory code for GCS_GDA_1994.
# Comes from https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/spatialreference.htm
spatialRefGDA94 = arcpy.SpatialReference(4283)
spatialRefMGAZone55 = arcpy.SpatialReference(28355)

# ######################################################################################################################
#
# Get centroids of meshblocks to use as point origins
#
# ######################################################################################################################

# Extract centroids of meshblocks to points
print('Extracting centroids of origin polygons')

# Dev meshblocks
if arcpy.Exists(r"ACT_census_FCs\dev_MB_XY"):
    arcpy.Delete_management(r"ACT_census_FCs\dev_MB_XY")
if arcpy.Exists(r"ACT_census_FCs\dev_MB_XY_res"):
    arcpy.Delete_management(r"ACT_census_FCs\dev_MB_XY_res")

arcpy.FeatureToPoint_management(r"ACT_census_FCs\dev_ACT_MB", r"ACT_census_FCs\dev_MB_XY", "CENTROID")
# arcpy.FeatureClassToFeatureClass_conversion(r"ACT_census_FCs\dev_MB_XY", r"ACT_census_FCs", "dev_MB_XY_filt", filter_exp)
# arcpy.FeatureClassToFeatureClass_conversion(r"ACT_census_FCs\dev_ACT_MB", r"ACT_census_FCs", "dev_ACT_MB_filt", filter_exp)

# Real meshblocks
if arcpy.Exists(r"ACT_census_FCs\real_MB_XY"):
    arcpy.Delete_management(r"ACT_census_FCs\real_MB_XY")

if arcpy.Exists(r"ACT_census_FCs\real_MB_XY_filt"):
    arcpy.Delete_management(r"ACT_census_FCs\real_MB_XY_filt")

arcpy.FeatureToPoint_management(r"ACT_census_FCs\ACT_abs_MB", r"ACT_census_FCs\real_MB_XY", "CENTROID")
arcpy.FeatureToPoint_management(r"ACT_census_FCs\MB_manual_selection", r"ACT_census_FCs\manual_XY", "CENTROID")
# arcpy.FeatureClassToFeatureClass_conversion(r"ACT_census_FCs\real_MB_XY", r"ACT_census_FCs", "real_MB_XY_filt", filter_exp)
# arcpy.FeatureClassToFeatureClass_conversion(r"ACT_census_FCs\ACT_abs_MB", r"ACT_census_FCs", "ACT_abs_MB_filt", filter_exp)



########################################################################################################################
#
# Gravity modelling begins. For every origin, compute distance and travel time to every destination via the network.
# Reference:
#   https://community.esri.com/t5/arcgis-network-analyst-questions/o-d-matrix-analysis-does-not-work-with-arcgis-pro/td-p/1074485
#   https://pro.arcgis.com/en/pro-app/latest/tool-reference/network-analyst/make-od-cost-matrix-analysis-layer.htm
#
########################################################################################################################

# Delete solver instances, previously manufactured layers
print("Pre-clean...")

if arcpy.Exists(r"C:\GIS\honours\dev\gravity_model\distanceMatrix.lyrx"):
    arcpy.Delete_management(r"C:\GIS\honours\dev\gravity_model\distanceMatrix.lyrx")

if arcpy.Exists(r"MBs_with_Costs"):
    arcpy.Delete_management(r"MBs_with_Costs")

ODsolvers = arcpy.ListDatasets("ODCostMatrixSolver*")
for solver in ODsolvers:
    print('Deleting ', solver)
    arcpy.Delete_management(solver)

# Set local parameters
if dev_mode:
    origins = r"ACT_census_FCs\dev_MB_XY"
    origin_poly = r"ACT_census_FCs\dev_ACT_MB"
    if mb_filters:
        origins = r"ACT_census_FCs\dev_MB_XY_filt"
        origin_poly = r"ACT_census_FCs\dev_ACT_MB_filt"
else:
    origins = r"ACT_census_FCs\real_MB_XY"
    origin_poly = r"ACT_census_FCs\ACT_abs_MB"
    if mb_filters:
        origins = r"ACT_census_FCs\manual_XY"
        origin_poly = r"ACT_census_FCs\MB_manual_selection"

destinations = r"wic_XY_FC"
network = r"network_dataset\actgov_roads_ND"
outfeat_name = "MBs_with_Costs"
outCSV = r"C:\GIS\honours\dev\gravity_model\distanceMatrix.csv"

# Get travel modes available on our network dataset.
travel_modes = arcpy.na.GetTravelModes(network)
travel_mode = travel_modes['Car']

outNALayerName = "distMatrix"

print("Creating OD layer...")

# Calculate the total distance, even though the analysis is optimizing time
accumulate_attributes = ["Length", "Minutes"]

# Create a new OD Cost matrix layer
result_object = arcpy.MakeODCostMatrixAnalysisLayer_na(network, outNALayerName, travel_mode,
                                                       accumulate_attributes=accumulate_attributes)
layer_object = result_object.getOutput(0)

# Get the names of all the sublayers within the OD cost matrix layer.
sublayer_names = arcpy.na.GetNAClassNames(layer_object)

# Stores the layer names that we will use later
origins_layer_name = sublayer_names["Origins"]
destinations_layer_name = sublayer_names["Destinations"]

# Map origin fields from input data to analysis layer to make it clearer what is going on
arcpy.AddFieldToAnalysisLayer_na(layer_object, origins_layer_name, "MB_ID", "TEXT")
arcpy.AddFieldToAnalysisLayer_na(layer_object, origins_layer_name, "Suburb", "TEXT")

field_mappings = arcpy.na.NAClassFieldMappings(layer_object, origins_layer_name)
field_mappings["MB_ID"].mappedFieldName = "MB_CODE21"
field_mappings["Suburb"].mappedFieldName = "SA2_NAME21"

# Load MBs as origin locations
arcpy.na.AddLocations(layer_object, origins_layer_name, origins, field_mappings)

# Map destination fields from input data to analysis layer to make it clearer what is going on
arcpy.AddFieldToAnalysisLayer_na(layer_object, destinations_layer_name, "WIC_ID", "TEXT")
arcpy.AddFieldToAnalysisLayer_na(layer_object, destinations_layer_name, "WIC_name", "TEXT")

field_mappings = arcpy.na.NAClassFieldMappings(layer_object, destinations_layer_name)
field_mappings["WIC_ID"].mappedFieldName = "ID"
field_mappings["WIC_name"].mappedFieldName = "disp_name"

# Load WICs as destination locations
arcpy.na.AddLocations(layer_object, destinations_layer_name, destinations, field_mappings)

stime = time.time()
# Solve the OD cost matrix layer
print("Solving...")
arcpy.na.Solve(layer_object)

print("Solution found in %.0f s" % (time.time() - stime))

# Transfer results from matrix to output features

# Get sublayers
origins_sublayer = arcpy.na.GetNASublayer(layer_object, "Origins")
destinations_sublayer = arcpy.na.GetNASublayer(layer_object, "Destinations")
lines_sublayer = arcpy.na.GetNASublayer(layer_object, "ODLines")

# Use the JoinField tool to transfer OD Cost Matrix information to the output feature class

# Transfer the MB ID from the input Origins to the output Lines
arcpy.JoinField_management(lines_sublayer, "OriginID", origins_sublayer, "ObjectID", ["MB_ID", "Suburb"])

# Transfer the hospital name from the input Destinations to the output Lines
arcpy.JoinField_management(lines_sublayer, "DestinationID", destinations_sublayer, "ObjectID", ["WIC_ID", "WIC_name"])

# Save results out to layer

print("Saving layer file")
# Save the solved OD cost matrix layer as a layer file on disk
outputname = r"C:\GIS\honours\dev\gravity_model\distanceMatrix.lyrx"
layer_object.saveACopy(outputname)
print("Saved to: ", outputname)

fields = ["MB_ID", "Suburb", "WIC_ID", "WIC_name", "Total_Length", "Total_Minutes"]

# Exporting to CSV lets us then bring it back into Pandas for data transformations.
print("Saving lines to CSV...")
with open(outCSV, 'w') as f:
    f.write(','.join(fields) + '\n')  # csv headers
    with arcpy.da.SearchCursor(lines_sublayer, fields) as cursor:
        for row in cursor:
            f.write(','.join([str(r) for r in row]) + '\n')

# Reshape data ready to join back to results tables
reshaped_csvs = mt.reshape_OD_ouput(root, r"\distanceMatrix.csv", length=True, time=True)

# Load tables into gdb
arcpy.TableToTable_conversion(reshaped_csvs[0], workspace, 'distMatTab')
arcpy.TableToTable_conversion(reshaped_csvs[1], workspace, 'timeMatTab')

# ######################################################################################################################
#
# Network processing complete. Now to move onto straight field calculations
#
# ######################################################################################################################

# Generate results table by taking copy of origins table then adding columns
# See 'Scratchpad diagram' https://gisgeography.com/huff-gravity-model/

# Copy origins FC to new results FC
print('Constructing results table')

if arcpy.Exists(r"results"):
    arcpy.Delete_management(r"results")

arcpy.CopyFeatures_management(origin_poly, "results")

# Change datatype of MB_ID in matrix tables from Double to Text
arcpy.AddField_management('distMatTab', 'MB_ID_txt','TEXT')
arcpy.CalculateField_management('distMatTab','MB_ID_txt','!MB_ID!','PYTHON3')

arcpy.AddField_management('timeMatTab', 'MB_ID_txt','TEXT')
arcpy.CalculateField_management('timeMatTab','MB_ID_txt','!MB_ID!','PYTHON3')

# Join distance matrix table onto results table
print('Join distance matrix to results table')
arcpy.JoinField_management("results", "MB_CODE21", "distMatTab", "MB_ID_txt")
arcpy.JoinField_management("results", "MB_CODE21", "timeMatTab", "MB_ID_txt")

# Add one field for attractiveness for each WIC, and calculate that value:
# NOTE: The attractiveness values are *really* small, so need double precision fields to store them
dist_fields = arcpy.ListFields('results', 'dist_wic_*')
time_fields = arcpy.ListFields('results', 'time_wic_*')

print('Calculating attractiveness...')
for field in dist_fields:
    name = field.name
    new_field_name = name.split('_')
    new_field_name = 'dist_attract_' + new_field_name[1] + '_' + new_field_name[2]
    arcpy.AddField_management('results', new_field_name, 'DOUBLE')

    expression = "compute_attract(!" + name + "!)"

    # Assume that all WICs equally attractive, and that gravity exponent is 2

    codeblock = '''
def compute_attract(distance):
    principle = 1
    exponent = 2
    attract = principle / float(distance) ** exponent
    return attract
'''
    arcpy.CalculateField_management('results', new_field_name, expression, 'PYTHON3', codeblock)

for field in time_fields:
    name = field.name
    new_field_name = name.split('_')
    new_field_name = 'time_attract_' + new_field_name[1] + '_' + new_field_name[2]
    arcpy.AddField_management('results', new_field_name, 'DOUBLE')

    expression = "compute_attract(!" + name + "!)"

    # Assume that all WICs equally attractive, and that gravity exponent is 2

    codeblock = '''
def compute_attract(time):
    principle = 1
    exponent = 2
    attract = principle / float(time) ** exponent
    return attract
'''
    arcpy.CalculateField_management('results', new_field_name, expression, 'PYTHON3', codeblock)

# Compute total attractiveness column:

print('Calculating total attractiveness')
arcpy.AddField_management('results', 'total_dist_attract', 'DOUBLE')
arcpy.AddField_management('results', 'total_time_attract', 'DOUBLE')

dist_attract_fields = [field.name for field in arcpy.ListFields('results', 'dist_attract_*')]
time_attract_fields = [field.name for field in arcpy.ListFields('results', 'time_attract_*')]

# Generate expression that produces sum of attract columns
expression = ''
for field in dist_attract_fields:
    field = '!' + field + '!,'
    expression = expression + field

expression = 'sum([' + expression.rstrip(',') + '])'

arcpy.CalculateField_management('results', 'total_dist_attract', expression, 'PYTHON3')

expression = ''
for field in time_attract_fields:
    field = '!' + field + '!,'
    expression = expression + field

expression = 'sum([' + expression.rstrip(',') + '])'

arcpy.CalculateField_management('results', 'total_time_attract', expression, 'PYTHON3')

# Add one field for likelihood for each WIC, and calculate that value:

print('Calculating likelihood of interaction...')
for field in dist_attract_fields:
    new_field_name = field.split('_')
    new_field_name = 'dist_likelihood_' + new_field_name[2] + '_' + new_field_name[3]
    arcpy.AddField_management('results', new_field_name, 'DOUBLE')

    expression = '!' + field + '!/!total_dist_attract!'

    arcpy.CalculateField_management('results', new_field_name, expression, 'PYTHON3')

for field in time_attract_fields:
    new_field_name = field.split('_')
    new_field_name = 'time_likelihood_' + new_field_name[2] + '_' + new_field_name[3]
    arcpy.AddField_management('results', new_field_name, 'DOUBLE')

    expression = '!' + field + '!/!total_time_attract!'

    arcpy.CalculateField_management('results', new_field_name, expression, 'PYTHON3')

print("Model complete. Total time %.0f s" % (time.time() - rtime))
"""
preprocess_data.py

Script to perform geoprocessing tasks on input data to prepare for use in a gravity model

Version: v0
Date: 2021
Author: Michael Vernon
"""

import arcpy
import sys
import os
import shutil

# Set environment variables
workspace = r"C:\GIS\honours\dev\dev_proj\dev_proj.gdb"
arcpy.env.workspace = workspace
arcpy.env.scratchWorkspace = workspace
arcpy.env.overwriteOutput = True

# Set spatial reference. '4283' is the factory code for GCS_GDA_1994.
# Comes from https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/spatialreference.htm
print('Setting spatial refs')
spatialRefGDA94 = arcpy.SpatialReference(4283)
spatialRefMGAZone55 = arcpy.SpatialReference(28355)

# # Get available features
# print('Getting features...')
# FCs = arcpy.ListFeatureClasses()
# print('Found feature classes', FCs)
# tabs = arcpy.ListTables()
# print('Found tables', tabs)
#
# # Extract geocoded service locations
# geocode_tab = [x for x in tabs if 'geocode' in x]
# print(geocode_tab)
#
# # This is still a list. Have to index it for the only entry to pass on to Arcpy
# print('Creating points from XY data')
# for tab in geocode_tab:
#     out_name = tab.split("_")
#     out_name = out_name[0] + "_XY"
#     arcpy.XYTableToPoint_management(tab, out_name, "long", "lat", None, spatialRefGDA94)
#
# # print('Adding attractiveness values')
# # # Insert additional column to handle attractiveness value. For now, set it to 1
# # arcpy.AddField_management("fast_food_XY", "attract", "FLOAT")
# # arcpy.CalculateField_management("fast_food_XY", "attract", "1.00", "PYTHON3", '', "FLOAT", "NO_ENFORCE_DOMAINS")
#
# # Add feature dataset to store ACT-trimmed features
# if arcpy.Exists("ACT_census_FCs"):
#     print('Deleting old dataset')
#     arcpy.Delete_management("ACT_census_FCs")
#
# print('Creating ACT dataset')
# arcpy.CreateFeatureDataset_management(workspace, "ACT_census_FCs", spatialRefMGAZone55)
#
# # Select by attribute on ABS FCs to get just ACT
# # Get list of ABS FCs
# abs_FCs = [x for x in FCs if 'abs' in x]
#
# query = "STE_NAME21 = 'Australian Capital Territory'"
# path = workspace + r"\ACT_census_FCs"
#
# print('Clipping ABS data to ACT boundary')
# for fc in abs_FCs:
#     # Add prefix of 'ACT', remove suffix of 'FC'
#     out_name = "ACT_" + fc
#     out_name = out_name.rstrip("_FC")
#
#     # Clip with query
#     arcpy.FeatureClassToFeatureClass_conversion(fc, path, out_name, query)
#
# # The default datatypes for the ACT SA1 FCs and SEIFA data don't match. To execute the join properly, we must
# # create a new field, then transfer all the data over... It is a bit of a pain but not too bad.
#
# print('Joining SEIFA data to SA1s')
# # Create new field:
# arcpy.AddField_management("abs_SEIFA_SA1", "SA1_code", "TEXT")
# arcpy.CalculateField_management("abs_SEIFA_SA1", "SA1_code", "!SA1_11!", "PYTHON3", '', "TEXT", "NO_ENFORCE_DOMAINS")
#
# # Add join from ACT SA1s to SEFIA data. Need to use JoinField, not AddJoin. JoinField permanently adds fields to input,
# # while AddJoin creates a temporary feature layer that is temporary and must be exported again later. Skip the hassle.
# arcpy.JoinField_management("ACT_abs_SA1", "SA1_CODE21", "abs_SEIFA_SA1", "SA1_code")
#
# # Now, the same to join population data to meshblocks. Fortunately, the data is already of the right type this time.
# # We are also specifying just two fields to join on, Dwelling and Persons to try keep the table size more manageable
# print('Joining population data to MBs')
# arcpy.JoinField_management("ACT_abs_MB", "MB_CODE21", "abs_counts_MB", "MB_CODE_2016", ["Dwelling", "Person"])

############################################### Network Analysis ######################################################

# Create new network dataset

if arcpy.Exists(r"network_dataset\tccs_roads_ND"):
    print('Deleting old ND')
    arcpy.Delete_management(r"network_dataset\tccs_roads_ND")

if arcpy.Exists(r"network_dataset\actgov_roads_ND"):
    print('Deleting old ND')
    arcpy.Delete_management(r"network_dataset\actgov_roads_ND")

print("Creating roads network dataset")
arcpy.CreateNetworkDataset_na(workspace + r"\network_dataset", "actgov_roads_ND", "actgov_roads_FC_for_ND", "NO_ELEVATION")

# Build network
try:
    print("Building network")
    arcpy.BuildNetwork_na(r"network_dataset\actgov_roads_ND")

    # If there are any build errors, they are recorded in a BuildErrors.txt file
    # present in the system temp directory. So copy this file to the directory
    # containing this script

    # First get the path to the system temp directory
    tempDir = os.environ.get("TEMP")
    if tempDir:
        shutil.copy2(os.path.join(tempDir, "BuildErrors.txt"), sys.path[0])

    print("Network Build Success")

except Exception as e:
    # If an error occurred, print line number and error message
    import traceback, sys

    tb = sys.exc_info()[2]
    print("An error occurred on line %i" % tb.tb_lineno)
    print(str(e))
'''
Use this after running _all_ the other scripts.
Takes outputs from both private and public models, and combines them with SEIFA data for analysis.
'''

import arcpy
from arcpy.sa import *
import time

# Start timer
rtime = time.time()

root = r"C:\GIS\honours\dev\gravity_model"
private_workspace = root + r"\gravity_model.gdb"
pt_workspace = root + r"\pt_gravity_res.gdb"
output_workspace = root + r"\model_outputs.gdb"

# Apply environment variables

arcpy.env.workspace = output_workspace
arcpy.env.scratchWorkspace = output_workspace
arcpy.env.overwriteOutput = True

# Set spatial reference. '4283' is the factory code for GCS_GDA_1994.
# Comes from https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/spatialreference.htm
spatialRefGDA94 = arcpy.SpatialReference(4283)
spatialRefMGAZone55 = arcpy.SpatialReference(28355)

# Reload output workspace
print("Creating database to model outputs {}".format(output_workspace))

if arcpy.Exists(output_workspace):
    arcpy.Delete_management(output_workspace)

arcpy.CreateFileGDB_management(root, "model_outputs.gdb")

# Combine day rasters into week rasters
print("Combining daily rasters")
arcpy.env.workspace = pt_workspace
combined_rasters = arcpy.ListRasters('*_combined')

min_rasters = [match for match in combined_rasters if "min" in match]
mean_rasters = [match for match in combined_rasters if "mean" in match]
max_rasters = [match for match in combined_rasters if "max" in match]

all_rasters = [min_rasters, mean_rasters, max_rasters]

out_sums = []
for stat in all_rasters:
    weekday = stat[0]
    friday = stat[1]
    saturday = stat[2]
    sunday = stat[3]

    # Build weighted sum table object - assign weighting based on proportion of week
    WSumTableObj = WSTable([[weekday, "VALUE", 4/7], [friday, "VALUE", 1/7], [saturday, "VALUE", 1/7],
                           [sunday, "VALUE", 1/7]])

    # Execute weighted sum operation
    weightedSumObject = WeightedSum(WSumTableObj)
    out_sums.append(weightedSumObject)

# Save to file
out_sums[0].save(output_workspace + r"\PT_minimum_week")
out_sums[1].save(output_workspace + r"\PT_mean_week")
out_sums[2].save(output_workspace + r"\PT_max_week")

# Turns out, this isn't all that useful. Instead, we will make outputs for weekday mean and weekend mean, and keep them
# separated.

# Combine Monday - Thursday + Friday
WSumTableObj = WSTable([[mean_rasters[0], "VALUE", 4/5], [mean_rasters[1], "VALUE", 1/5]])
weightedSumObject = WeightedSum(WSumTableObj)
weightedSumObject.save(output_workspace + r"\PT_mean_weekday")

# Combine Saturday + Sunday
WSumTableObj = WSTable([[mean_rasters[2], "VALUE", 1/2], [mean_rasters[3], "VALUE", 1/2]])
weightedSumObject = WeightedSum(WSumTableObj)
weightedSumObject.save(output_workspace + r"\PT_mean_weekend")

# Combine public transit and private
arcpy.env.workspace = output_workspace

print("Combining public_mean and private")
# Grab datasets
public_transport = output_workspace + r"\PT_mean_week"
private_transport = private_workspace + r"\comb_time"

# Build weighted sum table object - assign weighting based journey share
WSumTableObj = WSTable([[public_transport, "VALUE", 0.15], [private_transport, "VALUE", 0.85]])

# Execute weighted sum operation
weightedSumObject = WeightedSum(WSumTableObj)

# Save to file
weightedSumObject.save(output_workspace + r"\pvt_pt_combined")

# Invert mean PT week raster + pvt & PT raster

print("Inverting gravity rasters")
def invertRaster(raster):
    raster_min = float(arcpy.GetRasterProperties_management(raster, "MINIMUM").getOutput(0))
    raster_max = float(arcpy.GetRasterProperties_management(raster, "MAXIMUM").getOutput(0))

    inverted_raster = RasterCalculator([raster], ["x"], f"((x - {raster_max}) * -1) + {raster_min}")
    return inverted_raster

# Define targets for inversion
public_transport_weekday = output_workspace + r"\PT_mean_weekday"
public_transport_weekend = output_workspace + r"\PT_mean_weekend"
pub_pvt_combined = output_workspace + r"\pvt_pt_combined"
private_transport = private_workspace + r"\comb_time"

# Invert PT weekday
pub_week_inverted = invertRaster(public_transport_weekday)
pub_week_inverted.save(output_workspace + r"\PT_mean_weekday_inverted")

# Invert PT weekend
pub_weekend_inverted = invertRaster(public_transport_weekend)
pub_weekend_inverted.save(output_workspace + r"\PT_mean_weekend_inverted")

# Invert pt + pvt
combo_inverted = invertRaster(pub_pvt_combined)
combo_inverted.save(output_workspace + r"\pvt_pt_combined_inverted")

# Invert pvt
private_inverted = invertRaster(private_transport)
private_inverted.save(output_workspace + r"\pvt_inverted")

# *
# Standardize and invert SEIFA
# *
print("Messing with SEIFA")
# Load SA1 polygons with SEIFA data joined on
SA1_polygons = private_workspace + r"\ACT_census_FCs\ACT_abs_SA1"

# Load manual meshblocks to clip with
MB_mask = private_workspace + r"\ACT_census_FCs\MB_manual_selection"
SA1_polygons_clipped = private_workspace + r"\ACT_census_FCs\SA1_clipped_to_manual"

# Clip
arcpy.Clip_analysis(SA1_polygons, MB_mask, SA1_polygons_clipped)

# Convert to raster
cellsize = 10
arcpy.CreateRasterDataset_management(output_workspace, 'SEIFA_raster', cellsize=cellsize,
                                     raster_spatial_reference=spatialRefMGAZone55)

arcpy.PolygonToRaster_conversion(SA1_polygons_clipped, "IRSAD_Score", 'SEIFA_raster', cell_assignment='CELL_CENTER',
                                 cellsize=cellsize)
# Set 0 values to Null instead

outputSetNull = SetNull('SEIFA_raster', 'SEIFA_raster', "VALUE <= 0")
outputSetNull.save(private_workspace + r'\SEIFA_raster_nodata')

# Standardise

raster_max = float(arcpy.GetRasterProperties_management(private_workspace + r'\SEIFA_raster_nodata', "MAXIMUM").getOutput(0))
standardised_raster = RasterCalculator([private_workspace + r'\SEIFA_raster_nodata'], ["x"], f"x/{raster_max}")
standardised_raster.save(private_workspace + r"\SEIFA_raster_standardised")

# Invert

SEIFA_inverted = invertRaster(private_workspace + r"\SEIFA_raster_standardised")
SEIFA_inverted.save(output_workspace + r"\SEIFA_std_inverted")

# *
# Combine SEIFA + mean week raster, pvt & PT raster
# *

print("Combining SEIFA")
SEIFA = output_workspace + r"\SEIFA_std_inverted"
grav_mod1 = output_workspace + r"\PT_mean_weekday_inverted"
grav_mod2 = output_workspace + r"\PT_mean_weekend_inverted"
grav_mod3 = output_workspace + r"\pvt_pt_combined_inverted"
grav_mod4 = output_workspace + r"\pvt_inverted"

WSumTableObj = WSTable([[SEIFA, "VALUE", 0.5], [grav_mod1, "VALUE", 0.5]])
WSumTableObj2 = WSTable([[SEIFA, "VALUE", 0.5], [grav_mod2, "VALUE", 0.5]])
WSumTableObj3 = WSTable([[SEIFA, "VALUE", 0.5], [grav_mod3, "VALUE", 0.5]])
WSumTableObj4 = WSTable([[SEIFA, "VALUE", 0.5], [grav_mod4, "VALUE", 0.5]])

# Execute weighted sum operation
weightedSumObject = WeightedSum(WSumTableObj)
weightedSumObject2 = WeightedSum(WSumTableObj2)
weightedSumObject3 = WeightedSum(WSumTableObj3)
weightedSumObject4 = WeightedSum(WSumTableObj4)

# Save to file
weightedSumObject.save(output_workspace + r"\SEIFA_and_PT_weekday")
weightedSumObject2.save(output_workspace + r"\SEIFA_and_PT_weekend")
weightedSumObject3.save(output_workspace + r"\SEIFA_and_PT_and_pvt")
weightedSumObject4.save(output_workspace + r"\SEIFA_and_pvt")


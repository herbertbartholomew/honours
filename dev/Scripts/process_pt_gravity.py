# Set local environment and processing variables

import arcpy
import time

# Flag to choose residential vs all meshblocks
useAllMeshblocks = False

# Start timer
rtime = time.time()

root = r"C:\GIS\honours\dev\gravity_model"
workspace = root + r"\gravity_model.gdb"

if useAllMeshblocks:
    grav_workspace = root + r"\pt_gravity.gdb"
else:
    grav_workspace = root + r"\pt_gravity_res.gdb"

arcpy.env.workspace = grav_workspace
arcpy.env.scratchWorkspace = grav_workspace
arcpy.env.overwriteOutput = True

# Set spatial reference. '4283' is the factory code for GCS_GDA_1994.
# Comes from https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/spatialreference.htm
spatialRefGDA94 = arcpy.SpatialReference(4283)
spatialRefMGAZone55 = arcpy.SpatialReference(28355)

# Rasterize polygon results tables to generate combined model

# Load up study area polygon

study_area = workspace + r'\ACT_census_FCs\ACT_abs_state'

# Load up results table

results_feature = grav_workspace + r"\pt_results"

# For each WIC, generate a raster

weekday_min = [field.name for field in arcpy.ListFields(results_feature, 'Weekday_min_likelihood_*')]
weekday_mean = [field.name for field in arcpy.ListFields(results_feature, 'Weekday_mean_likelihood_*')]
weekday_max = [field.name for field in arcpy.ListFields(results_feature, 'Weekday_max_likelihood_*')]

Friday_min = [field.name for field in arcpy.ListFields(results_feature, 'Friday_min_likelihood_*')]
Friday_mean = [field.name for field in arcpy.ListFields(results_feature, 'Friday_mean_likelihood_*')]
Friday_max = [field.name for field in arcpy.ListFields(results_feature, 'Friday_max_likelihood_*')]

Saturday_min = [field.name for field in arcpy.ListFields(results_feature, 'Saturday_min_likelihood_*')]
Saturday_mean = [field.name for field in arcpy.ListFields(results_feature, 'Saturday_mean_likelihood_*')]
Saturday_max = [field.name for field in arcpy.ListFields(results_feature, 'Saturday_max_likelihood_*')]

Sunday_min = [field.name for field in arcpy.ListFields(results_feature, 'Sunday_min_likelihood_*')]
Sunday_mean = [field.name for field in arcpy.ListFields(results_feature, 'Sunday_mean_likelihood_*')]
Sunday_max = [field.name for field in arcpy.ListFields(results_feature, 'Sunday_max_likelihood_*')]

all_groups = [weekday_min, weekday_mean, weekday_max, Friday_min, Friday_mean, Friday_max, Saturday_min, Saturday_mean, Saturday_max, Sunday_min, Sunday_mean, Sunday_max]

print('Generating rasters from results table')

all_rasters = []
for group in all_groups:
    raster_group = []
    for field in group:
        # Create dataset to save raster, 10m cellsize
        cell = 10
        raster_name = field.replace('_likelihood', '_raster')
        raster_group.append(raster_name)
        print('Processing ', raster_name)

        if arcpy.Exists(raster_name):
            arcpy.Delete_management(raster_name)

        arcpy.CreateRasterDataset_management(grav_workspace, raster_name, cellsize=cell,
                                             raster_spatial_reference=spatialRefMGAZone55)

        arcpy.PolygonToRaster_conversion(results_feature, field, raster_name, cell_assignment='CELL_CENTER', cellsize=cell)
    all_rasters.append(raster_group)

# # Use cell statistics to combine the rasters into a single model
print('Doing cell statistics')
for group in all_rasters:
    cellStats = arcpy.sa.CellStatistics(group, 'MAXIMUM', 'NODATA')
    out_name = group[0].replace('_wic_1', '_combined')
    print("saving {}...".format(out_name))
    cellStats.save(out_name)

print("Post processing complete. Total time %.0f s" % (time.time() - rtime))
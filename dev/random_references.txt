ACT footpaths: https://www.data.act.gov.au/Transport/Footpaths-in-the-ACT/8u78-prdj
ACT on-road cycling: https://www.data.act.gov.au/Transport/On-Road-Cycling/6z3p-svmx
light rail stops: https://www.data.act.gov.au/Transport/Light-Rail-Stops/28a2-f2xq
light rail route: https://www.data.act.gov.au/Transport/Light-Rail-Route/aqwx-hqk9
bus routes: https://www.data.act.gov.au/Transport/Bus-Routes-New-Network-July-2020/nes3-jii6
bus stops: https://www.data.act.gov.au/Transport/Bus-Stops-New-Network-July-2020/vdh8-ug3k
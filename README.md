# Measuring access to healthcare:
## A GIS-based application of gravity modelling

This repository contains the code that I wrote for my honours project in Bachelor of Engineering in late 2021 - early 2022.

### Software:
These scripts were written for use with ESRI ArcGIS Pro 2.9, and the ArcPy python interface that ships with ArcGIS.

### Overview:
The scripts were used to automate the otherwise-tedious process of generating gravity models for specific health care
services in Canberra, Australia.

The Dev/Scripts directory is where anything of use resides. The scripts are grouped based on which phase of the modelling
process they were written for:

- Initial data ingest:
  - dev_import_data.py - import raw input files from shapefiles and excel sheets to File Geodatabase formats
  - dev_preprocess_data.py - Set common coordinate system on all inputs, and trim to ACT. Construct network datasets
- Private transport gravity model:
  - model_import_data.py - import results of dev_preprocess_data.py ready for modelling
  - dev_gravity_model.py - Perform the gravity modelling stage on the private travel dataset
  - process_gravity.py - Convert results to raster, and combine individual rasters to single model for study area
- Public transit gravity model:
  - pt_gravity.py - do the gravity modelling stage on the public transit dataset
  - process_pt_gravity.py - Convert results to raster, and combine individual rasters to single model for study area
- Utilities:
  - mytools.py - contains a function that uses a pandas dataframe to perform a pivot operation on data coming in via a CSV
  - GIS_operators/ - very short python scripts that were passed directly to ArcGIS tools to perform field calculations
  - bigRun.py - script that queued a series of long-compute-time operations together so that I could run them overnight
  - final_assembly.py - script that combines rasters from both public and private models, and connects SEIFA data


### A cautionary note:
These scripts are by no-means plug and play. The quality of the code generally improves with time, as I learned better
approaches to handling data in ArcGIS. I hope to come back to this project in the future and clean up the repository,
but for now it just all here so I can link to this repo in my thesis rather than trying to include several thousand
lines of code as an appendix.

## Licensing
Copyright 2022 Michael Vernon

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
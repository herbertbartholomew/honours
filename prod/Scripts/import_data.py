"""
import_data.py

Script to import tabular and spatial data into their ESRI geodatabase equivalent formats, and save them into said
geodatabase.

Version: v0
Date: 2021
Author: Michael Vernon
"""

print('WARNING: This script will overwrite data in ', workspace)
input("Press Enter to continue...")

import arcpy

# Set environment variables
workspace = r"C:\GIS\honours\prod\MyProject\MyProject.gdb"
arcpy.env.workspace = workspace
arcpy.env.scratchWorkspace = workspace
arcpy.env.overwriteOutput = True

# Set spatial reference. '4283' is the factory code for GCS_GDA_1994.
# Comes from https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/spatialreference.htm
spatialRefMGAZone55 = arcpy.SpatialReference(28355)

print('WARNING: This script will overwrite data in ', workspace)
input("Press Enter to continue...")

# Create list of input files
input_base = r"C:\GIS\honours\prod\MyProject\InputData"

# Shapefiles
abs_MB = input_base + r"\shapefile\MB_2021_AUST_SHP_GDA94\MB_2021_AUST_GDA94.shp"
abs_SA1 = input_base + r"\shapefile\SA1_2021_AUST_SHP_GDA94\SA1_2021_AUST_GDA94.shp"
abs_SA2 = input_base + r"\shapefile\SA2_2021_AUST_SHP_GDA94\SA2_2021_AUST_GDA94.shp"
abs_SA3 = input_base + r"\shapefile\SA3_2021_AUST_SHP_GDA94\SA3_2021_AUST_GDA94.shp"
abs_state = input_base + r"\shapefile\STE_2021_AUST_SHP_GDA94\STE_2021_AUST_GDA94.shp"
actgov_roads = input_base + r"\shapefile\ACTGOV_ROADS_fixed\ACTGOV_ROADS_fixed.shp"

# Tabular - List with [0] as path, [1] as target sheet
wic_geocode = [input_base + r"\tabular\wic_manual_geocode.xlsx", 'Sheet1']
ed_geocode = [input_base + r"\tabular\ed_manual_geocode.xlsx", 'Sheet1']
chc_geocode = [input_base + r"\tabular\chc_manual_geocode.xlsx", 'Sheet1']

abs_SEIFA_SA1 = [input_base + r"\tabular\2033055001 - sa1 indexes.xls", 'Table 1']
abs_counts_MB = [input_base + r"\tabular\2016 census mesh block counts.xls", 'Table 8']


# input_shapes = [abs_MB, abs_SA1, abs_SA2, abs_SA3, abs_state]
# input_tabs = [wic_geocode, ed_geocode, chc_geocode, abs_SEIFA_SA1, abs_counts_MB]
input_tabs = []
input_shapes = [actgov_roads]

# Add feature dataset for network analysis
if arcpy.Exists("network_dataset"):
    print('Deleting old dataset')
    arcpy.Delete_management("network_dataset")

print('Creating Network dataset')
arcpy.CreateFeatureDataset_management(workspace, "network_dataset", spatialRefMGAZone55)

# Convert each shapefile to a feature class

for shape in input_shapes:
    # Get name of input variable to use to name outputs. Kinda hacky
    shape_name = [k for k, v in locals().items() if v == shape][0]
    print('processing shape: ' + shape_name)
    arcpy.FeatureClassToFeatureClass_conversion(shape, workspace, shape_name + "_FC")

# Process networks separately because they need to end up in a different directory.
arcpy.FeatureClassToFeatureClass_conversion(actgov_roads, workspace + r"\network_dataset", "actgov_roads" + "_FC_for_ND")

# Convert each excel workbook to a table
for tab in input_tabs:
    # Get name of input variable to use to name outputs. Kinda hacky
    tab_name = [k for k, v in locals().items() if v == tab][0]
    print('processing tabular: ' + tab_name)
    arcpy.ExcelToTable_conversion(tab[0], tab_name, tab[1])

# Print results
output_shapes = arcpy.ListFeatureClasses()
output_tabs = arcpy.ListTables()

print('\nFound', len(output_shapes), 'feature classes:')
for shape in output_shapes:
    print(shape)

print('\nFound', len(output_tabs), 'tables:')
for tab in output_tabs:
    print(tab)

print('\nFind them in', workspace)
